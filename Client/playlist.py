import json
import random
import requests
from dotenv import load_dotenv
import os
from get_data import GetData
load_dotenv()

# On crée la playlist
def Playlist():
    list_artist = GetData()[0]
    list_weight = GetData()[1]
    playlist ={'playlist' :[] }
    for x in random.choices(list_artist,list_weight, k=20):
        playlist['playlist'].append(requests.get(f"{os.getenv('SERVER_URL')}/random/{x}").json())
    return playlist


