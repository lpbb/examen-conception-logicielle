# Client



In order to launch the app, you first need to launch the Webservice. To do so, check the webservice directory.  

Once it's launched, use the **.env** file to inform the url the api gave you.  
Add your json file in the **data** directory and inform the name of the file in the **.env** file.  

Once it's done, after making sure you are in the **Client** directory you can launch the app using :
```
python main.py
```


The app will create a json file named **playlist** in the **data** directory containing the generated playlist.


### Tests:

Simple test to verify that basic methods are working.

After making sure you are in the **Client** directory, use :
```
pytest
```

(Not working on the sspcloud, i don't know why)