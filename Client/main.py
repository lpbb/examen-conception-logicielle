from playlist import Playlist
import json

# on appelle les bonnes fonctions et on crée le fichier json
playlist = Playlist()
with open("data/playlist.json", "w") as outfile:
        outfile.write(json.dumps(playlist, indent = 4))
