import json
from dotenv import load_dotenv
import os
load_dotenv()

# On récupère les données de rudy

def GetData():
    with open(f"data/{os.getenv('DATA')}.json", "r") as read_file:
        list_artist = []
        list_weight = []
        data = json.load(read_file)
        for music in data :
            list_artist.append(music['artiste'])
            list_weight.append(music['note'])
    return list_artist,list_weight
    



