from playlist import Playlist
from get_data import GetData

#test simple pour vefifier que le premier artiste de la playlist est bie dans le json de rudy
# On utilise lower pour pas avoir de problème de majuscule

def test_client():
    assert Playlist()['playlist'][0]['artist'].lower() in map(lambda x: x.lower(), GetData()[0])