# Conception Logicielle Exam

## Webservice

Simple API giving you information on random songs.  
Look at the Webservice readme in webservice directory to launch it.

## Client

Allows you to generate a playlist of random song given a artist json file.  
Look at the client readme in client directory to launch it.
