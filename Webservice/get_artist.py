import requests


def GetArtist(name : str):
    
    artist = requests.get(f"https://www.theaudiodb.com/api/v1/json/2/search.php?s={name}")
    return artist.json()['artists'][0]['idArtist']

