from get_album import GetAlbum
from get_artist import GetArtist


# Test si l'id d'un ablum de daft punk appartient bien à la liste d'identifiant d'album.

def test_webservice():
    assert '2120081' in GetAlbum(GetArtist('Daft Punk'))

