# Webservice

This is a simple API using [AudioDB](https://www.theaudiodb.com/api_guide.php) and [Lyricsovh](https://lyricsovh.docs.apiary.io/) to generate information on random songs.

### Architecture diagram :

```mermaid
graph LR
    B(Webservice)
    B --> C(AudioDB API)
    B --> D(lyrics.ovh API)
```

### To launch the API :

Use the following command line :

```
git clone https://gitlab.com/lpbb/examen-conception-logicielle.git
cd examen-conception-logicielle
pip install -r requirements.txt
cd Webservice
uvicorn main:app --reload
```

Once launched the status of the app is accessible on the url they gave.

If the app is working correctly, you can access random song of the artist you chose using:

```
Url_they_gave/random/artist_name
```


### Tests:

Simple test to verify that basic methods are working.

After making sure you are in the **Webservice** directory, use :
```
pytest
```

(Not working on the sspcloud, i don't know why)