import requests
import random
from get_album import GetAlbum
from get_artist import GetArtist



def GetSong(album_list : list):
    
    album = album_list[random.randint(0,len(album_list)-1)]
    track_list = requests.get(f"https://theaudiodb.com/api/v1/json/2/track.php?m={album}")
    L = track_list.json()['track']
    music = L[random.randint(0,len(L)-1)]
    artist =music["strArtist"]
    title =music["strTrack"]
    url_youtube = music["strMusicVid"]
    
    lyrics_json = requests.get(f"https://api.lyrics.ovh/v1/{artist}/{title}").json()
    if not('lyrics' in lyrics_json.keys()):
        lyrics = "no lyrics"
    else:
        lyrics = lyrics_json['lyrics']
    return {
        "artist":artist,
        "title":title,
        "suggested_youtube_url":url_youtube,
        "lyrics":lyrics 
     }


