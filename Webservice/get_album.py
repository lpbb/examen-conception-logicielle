import requests
from get_artist import GetArtist


def GetAlbum(id : str):
    
    album = requests.get(f"https://theaudiodb.com/api/v1/json/2/album.php?i={id}")
    L =[]
    for x in album.json()['album']:
        L.append(x['idAlbum'])
    return L

