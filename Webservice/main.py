from fastapi import FastAPI,HTTPException
from get_album import GetAlbum
from get_artist import GetArtist
from get_song import GetSong
app = FastAPI()


@app.get("/")
def read_root():
    try :
        GetSong(GetAlbum(GetArtist('Daft Punk')))
    except:
        raise HTTPException(status_code=424, detail="Failed Dependeny")
    return {"detail": "OK"}
    

@app.get("/random/{artist_name}")
def read_mot(artist_name : str):
    return GetSong(GetAlbum(GetArtist(artist_name)))

# a0luyo89_jsa3s4P8z7Yp91zZSive3YbTnpqQFpwq
# a0luyo89